package com.amity.abhishek.videos

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.MediaController
import android.widget.VideoView
import java.lang.reflect.Array.getLength
import android.R.attr.start
import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.os.AsyncTask
import android.util.Log
import org.w3c.dom.Attr
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import javax.xml.parsers.DocumentBuilderFactory
import android.os.AsyncTask.execute
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.core.view.GestureDetectorCompat
import android.content.Intent
import android.widget.Toast
import android.view.MotionEvent
import android.view.GestureDetector
import android.text.method.Touch.onTouchEvent






class MainActivity : AppCompatActivity()  {

    private var videoView: VideoView? = null
    private var gestureDetectorCompat: GestureDetectorCompat? = null
    var videos = arrayOf("rtsp://r3---sn-npoe7n7r.googlevideo.com/Cj0LENy73wIaNAn5CQutPMC2yBMYDSANFC3UboxdMOCoAUIASARgw5nAyeCTjKNbigELVGhsY2xRY21lc0EM/CBCDF6197A56F211E24213F46F629B9A5A11F202.2976D890FEDE01941A75ADBDE8DE624BB1AA5043/yt8/1/video.3gp",
        "rtsp://r5---sn-npoeenek.googlevideo.com/Cj0LENy73wIaNAnkVgdi5lxcPRMYDSANFC1Vc4xdMOCoAUIASARgw5nAyeCTjKNbigELVGhsY2xRY21lc0EM/586F0445B390FF171577B33711DE9598795C4FB4.994AAB1B281028AC33D13EF0195737EB87AA153C/yt8/1/video.3gp",
        "rtsp://r1---sn-npoe7ned.googlevideo.com/Cj0LENy73wIaNAmzUrlmqPBHthMYDSANFC11dYxdMOCoAUIASARgw5nAyeCTjKNbigELVGhsY2xRY21lc0EM/58F32EF590F463801A41A0BC0E22883C5F7A60AE.AA1121BE4C4D114AC45D0AABE4FF6C31D99299A9/yt8/1/video.3gp",
                "rtsp://r1---sn-npoe7nez.googlevideo.com/Cj0LENy73wIaNAlPvVQvM18LMBMYDSANFC3GeYxdMOCoAUIASARgkeng8-HTzaBdigELSy00enR0bXpvZkEM/A3E042A21E2841754B4D121B6C91AC4C386C325E.6B7830B1CE4322ED7ED92DC0C32B24204341F767/yt8/1/video.3gp",
                "rtsp://r4---sn-npoe7nez.googlevideo.com/Cj0LENy73wIaNAnvNUGIA99opRMYDSANFC03eoxdMOCoAUIASARgkeng8-HTzaBdigELSy00enR0bXpvZkEM/BAFF14D4DD1F0D70EEDCCE40DE52105FF573E998.E6B83424887C0FD1AB3E4EA38717832ECF81A9E6/yt8/1/video.3gp")
    var count:Int=2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        videoView = findViewById(R.id.videoView);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        gestureDetectorCompat = GestureDetectorCompat(this, MyGestureListener())
       startvideo()
    }
    override fun onTouchEvent(event: MotionEvent): Boolean {
        this.gestureDetectorCompat?.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    internal inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {

        override fun onFling(
            event1: MotionEvent, event2: MotionEvent,
            velocityX: Float, velocityY: Float
        ): Boolean {
            if (event2.y< event1.y) {
                if(count<5){
                    count=count+1
                    if(count>=5){
                        count=4

                    }

                    startvideo()
                }
            }
            else{
                if(count>=0){
                    count=count-1
                    if(count<0){
                        count=0

                    }
                    startvideo()
                }
            }

            return true
        }

    }
    fun startvideo(){
        videoView?.setVideoURI(Uri.parse(videos[count]))
        videoView?.setMediaController( MediaController(this));
        videoView?.requestFocus();
        videoView?.start();
    }

}
